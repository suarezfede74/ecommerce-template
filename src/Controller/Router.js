import React, { Component } from "react";
import Error from "../View/Error";
import Header from "../View/Header";
import Home from "../View/Home/Home";

export default class Router extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    if (this.props.error.error) {
      return <Error error={this.props.error} />;
    } else {
      return (
        <div>
          <Header />
          <Home />
        </div>
      );
    }
  }
}
