import React, { Component } from "react";
import Router from "../Controller/Router";//This component will be the main controller

class App extends Component {
  
  constructor(props) { // Props and state initialization 
    super(props);

    this.state = { 
      route: 'Home',
      error: {
        error: false,
        type: '',
        detail: ''
      },
    }
  }

  setRoute = (value) => {
    this.setState({route: value});
  }

  setError = (value, type, details) => {
    let auxError = {
      error: value,
      type: type,
      detail: details
    }
    if(value) console.error(type, details);
    this.setState({error: auxError});
  }

  render() {
    return <Router 
              route={this.state.route}
              setRoute={this.setRoute}
              error={this.state.error}
              setError={this.setError}
              />;
  }
}

export default App;
